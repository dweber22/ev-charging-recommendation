import csv
import random

# This script randomizes spawn times to be more linear
with open("dailyTraffic/trafficDemand.csv", "r") as data:
    spawn = list(csv.reader(data))

for row in spawn:
    row[0]=int(row[0])+random.randint(0,59)

with open("demand.csv", "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(spawn)
