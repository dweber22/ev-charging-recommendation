import csv
from datetime import datetime

# turns daily data(tuesday-monday) into single simpler file
# turns data from id, time, h3 index, trafficlevel into
# hour, h3 index, trafficlevel
minDem = []

with open("dailyTraffic/tuesday.csv", "r") as data:
    demandZones = list(csv.reader(data))
for row in demandZones:
    dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    minDem.append([dt.hour, row[2], row[3]])

with open("dailyTraffic/wednesday.csv", "r") as data:
    demandZones = list(csv.reader(data))
for row in demandZones:
    dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    minDem.append([dt.hour+24, row[2], row[3]])

with open("dailyTraffic/thursday.csv", "r") as data:
    demandZones = list(csv.reader(data))
for row in demandZones:
    dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    minDem.append([dt.hour+48, row[2], row[3]])

with open("dailyTraffic/friday.csv", "r") as data:
    demandZones = list(csv.reader(data))
for row in demandZones:
    dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    minDem.append([dt.hour+72, row[2], row[3]])

with open("dailyTraffic/saturday.csv", "r") as data:
    demandZones = list(csv.reader(data))
for row in demandZones:
    dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    minDem.append([dt.hour+96, row[2], row[3]])

with open("dailyTraffic/sunday.csv", "r") as data:
    demandZones = list(csv.reader(data))
for row in demandZones:
    dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    minDem.append([dt.hour+120, row[2], row[3]])

with open("dailyTraffic/monday.csv", "r") as data:
    demandZones = list(csv.reader(data))
for row in demandZones:
    dt = datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S')
    minDem.append([dt.hour+144, row[2], row[3]])

with open("dailyTraffic/week.csv", "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(minDem)
