import csv
import random

# This script adds fixed battery levels and wanted charge levels to the traffic demand
demand = []

def getSOC():
    ran=random.randint(0,1000)
    if(ran<100):
        return random.randint(50,199)
    if(ran<300):
        return random.randint(200,399)
    if(ran<700):
        return random.randint(400,599)
    if(ran<900):
        return random.randint(600,799)               
    return random.randint(800,950)

with open("demand.csv", "r") as data:
    dem = list(csv.reader(data))

for row in dem:
    bat=getSOC()
    # cars will always charge at least 10%, or fully (avg battery charged(incl overcharge)=40%)
    chargeTo=random.randint(bat+100, 1400)/10  
    demand.append([row[0], row[1], bat/10, chargeTo])

with open("demand.csv", "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(demand)
