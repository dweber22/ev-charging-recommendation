import h3
import csv
# reads file and flips long/lat in the hexagon
# dont run this if your data has correct hexagons

with open("dailyTraffic/sunday.csv", "r") as data:
    CSs = list(csv.reader(data))

# remove titles from rows
CSs.pop(0)

for index in CSs:
    wrong = h3.h3_to_geo(index[2])
    index[2] = h3.geo_to_h3(wrong[1], wrong[0], 7)


with open("dailyTraffic/sunday.csv", "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(CSs)
