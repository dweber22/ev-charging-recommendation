import h3
import csv

# reads file and replaces long/lat with r15 index and removes total power from data

with open("berlin_CSs.csv", "r") as data:
    CSs = list(csv.reader(data))

cs = []
CSs[0][0] = CSs[0][0][3:]
for index in CSs:
    location = h3.geo_to_h3(float(index[1]), float(index[0]), 15)
    cs.append([location, index[3], index[4]])


with open("CSs.csv", "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(cs)
