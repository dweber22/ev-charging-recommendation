import h3
import csv
import random


# returns a random child of the hexagon at input resolution (warning, slow (0.1s from res 7 to res 13))
def getChild(hexagon, res):
    children = list(h3.h3_to_children(hexagon, res))
    return random.choice(children)


with open("dailyTraffic/week.csv", "r") as data:
    traffic = list(csv.reader(data))

cars = []
# factor to multiply demand level with, adjust according to ev density
factor = 0.03
# keeps track of #cars generated
total = 0
# create list of cars generated
for row in traffic:
    # filter out streets that are completely blocked
    if row[2] == "10.0":
        continue
    # spawn n cars with jam*factor=n.xy
    for i in range(0, int(float(row[2])*factor)):
        cars.append([int(row[0])*60, getChild(row[1], 13)])
        total += 1
        print(total)
    odds = float(row[2])*factor-int(float(row[2])*factor)
    # spawn extra car with xy chance
    if(odds > random.random()):
        cars.append([int(row[0])*60, getChild(row[1], 13)])
        total += 1
        print(total)


with open("dailyTraffic/trafficDemand.csv", "w", newline="") as f:
    writer = csv.writer(f)
    writer.writerows(cars)
