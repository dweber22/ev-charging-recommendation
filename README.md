# EV Charging Recommendation

EV charging recommendation simulations for my bachelor thesis.

# Required Data

Csv with a row for each car including:
- time (in minutes, starting from 0)

- h3 index 

- battery level

- intended charge level

Csv with charging stations including:

- h3 index

- number of plugs

- average power per plug

Rename your files according to the files in the code (demand.csv for cars and CSs.csv for CSs) or rename the files in the code.

# Transforming Data

To help with transforming the data into the required format these scripts (dataTransformation folder) can be used:

- fixHexagon.py flips the long and lat attributes of the h3 indexes, since my data had them flipped. Adjust filename and list index as necessary.

- improveData.py shrinks down files from tuesday-monday into one file and turns the data into one that demandgeneration can use.

- demandGeneration.py creates charging demand out of a csv with rows time (in hours, starting from 0), h3 index, traffic level(0-10))

- addTimeVariance.py adds 0-59 minutes on all times, so cars dont get spawned in batches of 60 minutes

- batAndCharge.py adds battery levels and intended charge level to the demand (seperate script because demand generation is really slow)

- locationToIndex.py replaces the CSs long/lat with index and removes unnecessary rows

# Set Up

To get set up, download the repo, replace the traffic files if you want and generate the conda environment from the textfile.

Run main.py to get your results via command line. Parameters can be adjusted at the start of the main.py file.
