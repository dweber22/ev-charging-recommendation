import simpy
import random
import h3
import csv
import statistics

# List of all drive times to calculate mean/median
driveTimes = []
# List of all queue times to calculate mean/median
queueTimes = []
# List of all charge times to calculate mean/median
chargeTimes = []
# List of all combined wait times to calculate mean/median
waitTimes = []


# parameters
# random, random free, closest, closest free, fastest free, lowest wait
csChoice = "closest"
# average speed in kmh
speed = 20
# turn into km per minute
speed = speed/60
# consumption in kwh/100km
consumption = 19.2
# turn into consumption in kwh/km
consumption = consumption/100
# battery size in kwh
batterySize = 83.7
# conversion rate so battery level 100=battery size and x kwh * conv = battery% used
batteryConversion = 100/batterySize
# the average car (demand.csv) charges 50%(incl. overcharge) + distance driven (usually 0.2-0.4kwh extra) in kwH
avgCharge = 0.5*batterySize+0.3
# percantage of drivers following the recommendation
recs=100


def car(env, car_id):
    # timeout since cars generate every hour, not all at once
    yield env.timeout(int(cars[car_id][0]))
    name = f'Car {car_id+1}'
    cs_id = pickCS(cars[car_id], csChoice)
    cs = csRes[cs_id]
    csname = f"cs{cs_id+1}"
    chargingSpeed = float(CSs[cs_id][2])
    battery_level = float(cars[car_id][2])
    chargeUntil = float(cars[car_id][3])
    driving_distance = distanceToCS(
        cars[car_id][1], CSs[cs_id][0])
    # assumes all cars drive a constant speed in a straight line
    driving_duration = driving_distance/speed
    createdAt = env.now

    # Simulate driving to the BCS
    yield env.timeout(driving_duration)
    battery_level -= driving_distance*consumption*batteryConversion
    # Request one of its charging spots
    print(f'[{round(env.now,2)}] {name}: arriving at {csname}')
    with cs.request() as req:
        startQ = env.now
        yield req
        timeQueued = env.now-startQ
        # Charge the battery
        print(
            f'[{round(env.now,2)}] {name}: starting to charge with {battery_level}% battery left')
        cTime = ((chargeUntil-battery_level) /
                 (chargingSpeed*batteryConversion))*60
        yield env.timeout(cTime)
        if chargeUntil>100:
            chargeUntil=100
        print(
            f'[{round(env.now,2)}] {name}: leaving {csname} with {chargeUntil}% battery')
        cs.release(req)
        global driveTimes
        driveTimes.append(driving_duration)
        global queueTimes
        queueTimes.append(timeQueued)
        global chargeTimes
        chargeTimes.append(cTime)
        global waitTimes
        waitTimes.append(env.now-createdAt)


def distanceToCS(carIndex, csIndex):
    return h3.point_dist(h3.h3_to_geo(carIndex), h3.h3_to_geo(csIndex), unit='km')


def pickCS(car, pickType):
    if(pickType == "random"):
        if(random.randint(1,100)>recs):
            return(pickCS(car,"closest free"))
        choices = []
        for cs in CSs:
            dist = distanceToCS(car[1], cs[0])
            # if cs in range add to potential choices
            if(dist*consumption*batteryConversion < float(car[2])):
                choices.append(cs)
        # if no CS is actually in reach, just take the closest one (unlikely to happen with realistic charging behavior)
        if(len(choices) == 0):
            return pickCS(car, "closest")
        return CSs.index(choices[random.randint(0, len(choices)-1)])

    if(pickType == "random free"):
        if(random.randint(1,100)>recs):
            return(pickCS(car,"closest free"))
        choices = []
        for cs in CSs:
            dist = distanceToCS(car[1], cs[0])
            # if cs in range and free add to potential choices
            if(dist*consumption*batteryConversion < float(car[2]) and csRes[CSs.index(cs)].count < csRes[CSs.index(cs)].capacity):
                choices.append(cs)
        # if no free CS is in range, pick the closest one (only rarely happens)
        if(len(choices) == 0):
            return pickCS(car, "closest")
        return CSs.index(choices[random.randint(0, len(choices)-1)])

    if(pickType == "closest"):
        if(random.randint(1,100)>recs):
            return(pickCS(car,"closest free"))
        smallest = 1000000
        choices=[]
        for cs in CSs:
            dist = distanceToCS(car[1], cs[0])
            if(dist == smallest):
                choices.append(cs)
            elif(dist < smallest):
                smallest = dist
                choices=[cs]
        # if multiple CSs have same results, choose a random one
        return CSs.index(random.choice(choices))
    if(pickType == "closest free"):
        choices = []
        smallest = 1000000
        for cs in CSs:
            dist = distanceToCS(car[1], cs[0])
            if(dist == smallest and csRes[CSs.index(cs)].count < csRes[CSs.index(cs)].capacity):
                choices.append(cs)
            elif(dist < smallest and csRes[CSs.index(cs)].count < csRes[CSs.index(cs)].capacity):
                smallest = dist
                choices = [cs]
        # if the car cant reach CS because battery is too low, it can't be chosen
        if smallest*consumption*batteryConversion < float(car[2]) and choices:
            return CSs.index(random.choice(choices))
        return pickCS(car, "closest")

    if(pickType == "fastest free"):
        if(random.randint(1,100)>recs):
            return(pickCS(car,"closest free"))
        choices = []
        cSpeed = 0
        for cs in CSs:
            dist = distanceToCS(car[1], cs[0])
            if csRes[CSs.index(cs)].count < csRes[CSs.index(cs)].capacity and float(cs[2]) == cSpeed and dist*consumption*batteryConversion < float(car[2]):
                choices.append(cs)
            elif csRes[CSs.index(cs)].count < csRes[CSs.index(cs)].capacity and float(cs[2]) > cSpeed and dist*consumption*batteryConversion < float(car[2]):
                cSpeed = float(cs[2])
                choices = [cs]
        if choices:    
            return CSs.index(random.choice(choices))
        return pickCS(car, "closest")

    if(pickType == "lowest wait"):
        if(random.randint(1,100)>recs):
            return(pickCS(car,"closest free"))
        choices = []
        time = 1000000
        for cs in CSs:
            dist = distanceToCS(car[1], cs[0])
            dTime = dist/speed
            if(csRes[CSs.index(cs)].count < csRes[CSs.index(cs)].capacity):
                qTime = 0
            else:
                chargeTimePerCar = avgCharge/float(cs[2])*60
                # +1 because when q empty all plugs are still filled
                qTime = (len(csRes[CSs.index(cs)].queue)+1)*chargeTimePerCar
            batNeeded = float(car[3])-float(car[2])
            cTime = (batNeeded+dist*consumption*batteryConversion)/float(cs[2]) *60
            tTime = dTime+qTime+cTime
            if tTime == time and dist*consumption*batteryConversion < float(car[2]):
                choices.append(cs)
            # if fastest and reachable
            elif tTime < time and dist*consumption*batteryConversion < float(car[2]):
                time = tTime
                choices = [cs]
        # if no CS is in range, the closest CS will be used
        if choices:
            return CSs.index(random.choice(choices))
        return pickCS(car, "closest")


env = simpy.Environment()
with open("CSs.csv", "r") as data:
    CSs = list(csv.reader(data))
    # CSs[c][longitude/latitude/total power/number plugs/speed per plug]

# create CS resources and put them in a list for future access
csRes = []
for cs in CSs:
    csRes.append(simpy.Resource(env, capacity=int(cs[1])))

with open("demand.csv", "r") as data:
    cars = list(csv.reader(data))

# create cars
for i in range(len(cars)):
    env.process(car(env, i))

# env ends after all cars are generated
env.run()
# prepare output
meanDriveTime = statistics.mean(driveTimes)
meanQueueTime = statistics.mean(queueTimes)
meanChargeTime = statistics.mean(chargeTimes)
meanWaitTime = statistics.mean(waitTimes)

medianDriveTime = statistics.median(driveTimes)
medianQueueTime = statistics.median(queueTimes)
medianChargeTime = statistics.median(chargeTimes)
medianWaitTime = statistics.median(waitTimes)

# output
print("Mean time spent driving: " + str(round(meanDriveTime, 2)))
print("Mean time spent queueing: " + str(round(meanQueueTime, 2)))
print("Mean time spent charging: " + str(round(meanChargeTime, 2)))
print("Mean time spent total: " + str(round(meanWaitTime, 2)))


print("Median time spent driving: " + str(round(medianDriveTime, 2)))
print("Median time spent queueing: " + str(round(medianQueueTime, 2)))
print("Median time spent charging: " + str(round(medianChargeTime, 2)))
print("Median time spent total: " + str(round(medianWaitTime, 2)))

print(f"The CS selection type was {csChoice}")
